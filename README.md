# Nylnook A5 Comic templates

Templates for creating comics with [Krita](https://krita.org) and [Inkscape](https://inkscape.org) in a A5 paper format, as explained [in this detailed blog post](http://nylnook.com/en/blog/making-a-comic-from-A-to-Z-with-free-software).

## Installation for Krita

To install them, unzip, and paste the folder ( templates ) into your Krita user preference directory ( example on linux : ~/.local/share/krita )  
You can also open your preference directory inside Krita : "Edit > Ressources > Open Resources Folder" , don't forget to restart Krita after.  

Then you can choose one of the template when you create a new document under the "Nylnook" tab

## Installation for Inkscape

Just copy the "Comic_page_A5+.svg" file into your Inkscape user preference template directory (example on Linux : ~/./config/inkscape/templates, and on Windows in program files under inkscape/share/templates).  

Then you can create a new document with this template from the "File > Templates" menu, and choosing "Comic page A5+".


## Changelog

**September 23th 2016**: First release.


## License

CC-0 / Public Domain. Credit *Camille Bissuel* if needed. 

See my work at [nylnook.com](http://nylnook.com)


